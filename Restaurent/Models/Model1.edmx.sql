
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/05/2017 20:05:00
-- Generated from EDMX file: c:\users\asus_twy\documents\visual studio 2017\Projects\Restaurent\Restaurent\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Restaurent];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CustomerSet'
CREATE TABLE [dbo].[CustomerSet] (
    [C_Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [E_mail] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AdministratorSet'
CREATE TABLE [dbo].[AdministratorSet] (
    [A_Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [E_mail] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LoginSet'
CREATE TABLE [dbo].[LoginSet] (
    [User_Id] nvarchar(max)  NOT NULL,
    [C_id] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ReservationSet'
CREATE TABLE [dbo].[ReservationSet] (
    [R_Id] int IDENTITY(1,1) NOT NULL,
    [Res_Id] nvarchar(max)  NOT NULL,
    [T_Id] nvarchar(max)  NOT NULL,
    [R_time] nvarchar(max)  NOT NULL,
    [R_date] nvarchar(max)  NOT NULL,
    [C_id] nvarchar(max)  NOT NULL,
    [RD_time] nvarchar(max)  NOT NULL,
    [RD_date] nvarchar(max)  NOT NULL,
    [Customer_C_Id] int  NOT NULL
);
GO

-- Creating table 'TableSet'
CREATE TABLE [dbo].[TableSet] (
    [T_id] int IDENTITY(1,1) NOT NULL,
    [Res_id] nvarchar(max)  NOT NULL,
    [Reserved] nvarchar(max)  NOT NULL,
    [Capacoty] nvarchar(max)  NOT NULL,
    [RestaurantRes_Id] int  NOT NULL,
    [Customer_C_Id] int  NOT NULL
);
GO

-- Creating table 'RestaurantSet'
CREATE TABLE [dbo].[RestaurantSet] (
    [Res_Id] int IDENTITY(1,1) NOT NULL,
    [Res_name] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [ContactNumber] nvarchar(max)  NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [E_mail] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CustomerAdministrator'
CREATE TABLE [dbo].[CustomerAdministrator] (
    [Customer_C_Id] int  NOT NULL,
    [Administrator_A_Id] int  NOT NULL
);
GO

-- Creating table 'LoginCustomer'
CREATE TABLE [dbo].[LoginCustomer] (
    [Login_User_Id] nvarchar(max)  NOT NULL,
    [Customer_C_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [C_Id] in table 'CustomerSet'
ALTER TABLE [dbo].[CustomerSet]
ADD CONSTRAINT [PK_CustomerSet]
    PRIMARY KEY CLUSTERED ([C_Id] ASC);
GO

-- Creating primary key on [A_Id] in table 'AdministratorSet'
ALTER TABLE [dbo].[AdministratorSet]
ADD CONSTRAINT [PK_AdministratorSet]
    PRIMARY KEY CLUSTERED ([A_Id] ASC);
GO

-- Creating primary key on [User_Id] in table 'LoginSet'
ALTER TABLE [dbo].[LoginSet]
ADD CONSTRAINT [PK_LoginSet]
    PRIMARY KEY CLUSTERED ([User_Id] ASC);
GO

-- Creating primary key on [R_Id], [Res_Id], [T_Id] in table 'ReservationSet'
ALTER TABLE [dbo].[ReservationSet]
ADD CONSTRAINT [PK_ReservationSet]
    PRIMARY KEY CLUSTERED ([R_Id], [Res_Id], [T_Id] ASC);
GO

-- Creating primary key on [T_id], [Res_id] in table 'TableSet'
ALTER TABLE [dbo].[TableSet]
ADD CONSTRAINT [PK_TableSet]
    PRIMARY KEY CLUSTERED ([T_id], [Res_id] ASC);
GO

-- Creating primary key on [Res_Id] in table 'RestaurantSet'
ALTER TABLE [dbo].[RestaurantSet]
ADD CONSTRAINT [PK_RestaurantSet]
    PRIMARY KEY CLUSTERED ([Res_Id] ASC);
GO

-- Creating primary key on [Customer_C_Id], [Administrator_A_Id] in table 'CustomerAdministrator'
ALTER TABLE [dbo].[CustomerAdministrator]
ADD CONSTRAINT [PK_CustomerAdministrator]
    PRIMARY KEY CLUSTERED ([Customer_C_Id], [Administrator_A_Id] ASC);
GO

-- Creating primary key on [Login_User_Id], [Customer_C_Id] in table 'LoginCustomer'
ALTER TABLE [dbo].[LoginCustomer]
ADD CONSTRAINT [PK_LoginCustomer]
    PRIMARY KEY CLUSTERED ([Login_User_Id], [Customer_C_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Customer_C_Id] in table 'CustomerAdministrator'
ALTER TABLE [dbo].[CustomerAdministrator]
ADD CONSTRAINT [FK_CustomerAdministrator_Customer]
    FOREIGN KEY ([Customer_C_Id])
    REFERENCES [dbo].[CustomerSet]
        ([C_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Administrator_A_Id] in table 'CustomerAdministrator'
ALTER TABLE [dbo].[CustomerAdministrator]
ADD CONSTRAINT [FK_CustomerAdministrator_Administrator]
    FOREIGN KEY ([Administrator_A_Id])
    REFERENCES [dbo].[AdministratorSet]
        ([A_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerAdministrator_Administrator'
CREATE INDEX [IX_FK_CustomerAdministrator_Administrator]
ON [dbo].[CustomerAdministrator]
    ([Administrator_A_Id]);
GO

-- Creating foreign key on [Login_User_Id] in table 'LoginCustomer'
ALTER TABLE [dbo].[LoginCustomer]
ADD CONSTRAINT [FK_LoginCustomer_Login]
    FOREIGN KEY ([Login_User_Id])
    REFERENCES [dbo].[LoginSet]
        ([User_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Customer_C_Id] in table 'LoginCustomer'
ALTER TABLE [dbo].[LoginCustomer]
ADD CONSTRAINT [FK_LoginCustomer_Customer]
    FOREIGN KEY ([Customer_C_Id])
    REFERENCES [dbo].[CustomerSet]
        ([C_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LoginCustomer_Customer'
CREATE INDEX [IX_FK_LoginCustomer_Customer]
ON [dbo].[LoginCustomer]
    ([Customer_C_Id]);
GO

-- Creating foreign key on [Customer_C_Id] in table 'ReservationSet'
ALTER TABLE [dbo].[ReservationSet]
ADD CONSTRAINT [FK_ReservationCustomer]
    FOREIGN KEY ([Customer_C_Id])
    REFERENCES [dbo].[CustomerSet]
        ([C_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservationCustomer'
CREATE INDEX [IX_FK_ReservationCustomer]
ON [dbo].[ReservationSet]
    ([Customer_C_Id]);
GO

-- Creating foreign key on [Customer_C_Id] in table 'TableSet'
ALTER TABLE [dbo].[TableSet]
ADD CONSTRAINT [FK_TableCustomer]
    FOREIGN KEY ([Customer_C_Id])
    REFERENCES [dbo].[CustomerSet]
        ([C_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TableCustomer'
CREATE INDEX [IX_FK_TableCustomer]
ON [dbo].[TableSet]
    ([Customer_C_Id]);
GO

-- Creating foreign key on [RestaurantRes_Id] in table 'TableSet'
ALTER TABLE [dbo].[TableSet]
ADD CONSTRAINT [FK_RestaurantTable]
    FOREIGN KEY ([RestaurantRes_Id])
    REFERENCES [dbo].[RestaurantSet]
        ([Res_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RestaurantTable'
CREATE INDEX [IX_FK_RestaurantTable]
ON [dbo].[TableSet]
    ([RestaurantRes_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------