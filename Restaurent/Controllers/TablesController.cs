﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Restaurent.Models;

namespace Restaurent.Controllers
{
    public class TablesController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: Tables
        public ActionResult Index()
        {
            var tableSet = db.TableSet.Include(t => t.Restaurant);
            return View(tableSet.ToList());
        }

        // GET: Tables/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table table = db.TableSet.Find(id);
            if (table == null)
            {
                return HttpNotFound();
            }
            return View(table);
        }

        // GET: Tables/Create
        public ActionResult Create()
        {
            ViewBag.RestaurantRes_Id = new SelectList(db.RestaurantSet, "Res_Id", "Res_name");
            return View();
        }

        // POST: Tables/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "T_id,Res_id,Reserved,Capacoty,RestaurantRes_Id")] Table table)
        {
            if (ModelState.IsValid)
            {
                db.TableSet.Add(table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RestaurantRes_Id = new SelectList(db.RestaurantSet, "Res_Id", "Res_name", table.RestaurantRes_Id);
            return View(table);
        }

        // GET: Tables/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table table = db.TableSet.Find(id);
            if (table == null)
            {
                return HttpNotFound();
            }
            ViewBag.RestaurantRes_Id = new SelectList(db.RestaurantSet, "Res_Id", "Res_name", table.RestaurantRes_Id);
            return View(table);
        }

        // POST: Tables/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "T_id,Res_id,Reserved,Capacoty,RestaurantRes_Id")] Table table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RestaurantRes_Id = new SelectList(db.RestaurantSet, "Res_Id", "Res_name", table.RestaurantRes_Id);
            return View(table);
        }

        // GET: Tables/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Table table = db.TableSet.Find(id);
            if (table == null)
            {
                return HttpNotFound();
            }
            return View(table);
        }

        // POST: Tables/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Table table = db.TableSet.Find(id);
            db.TableSet.Remove(table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
